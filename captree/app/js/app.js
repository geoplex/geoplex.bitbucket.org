// Defer until everything is loaded (fonts, scripts, models). Simplifies the demo.
$(window).load(function() {
  // Enable reflection on webkit browsers.
  //
  // On window resize or orientation change trigger visualization resize after a short delay.
  //
  var demoData = {
    a:{ data:"assets/data/helicopter.json", title: "Helicopter Example", color: "PassFail"},
    b:{ data:"assets/data/tr.json", title: "Test Range Example", color: "PassFail"},
    c:{ data:"assets/data/Beer.json", title: "Beer Example", color: "Random"}
  }

  var onSizeChange = (function() {
    var timeout;
    return function() {
      $("body").addClass("dimmed");
      window.clearTimeout(timeout);
      timeout = window.setTimeout(function() {
        if (circles) {
          updateMeta();
          circles.resize();
          $("body").removeClass("dimmed");
        }
      }, 250);
    };
  })();
  window.addEventListener("orientationchange", onSizeChange, false);
  window.addEventListener("resize", onSizeChange, false);

  var shiftKey = false;
  $(document).keydown(function (e) {
      if (e.keyCode == 16) {
          shiftKey = true;
      }
  });

  $(document).keyup(function (e) {
      if (e.keyCode == 16) {
          shiftKey = false;
      }
  });

  function fetchJSONFile(path, callback) {
      var httpRequest = new XMLHttpRequest();
      httpRequest.onreadystatechange = function() {
          if (httpRequest.readyState === 4) {
              if (httpRequest.status === 200) {
                  var data = JSON.parse(httpRequest.responseText);
                  if (callback) callback(data);
              }
          }
      };
      httpRequest.open('GET', path);
      httpRequest.send();
  }

  outlinePainter = function(opts, params, vars) {

      var deficiency = params.group.deficiency_plots;
      if (threshold != null) {
          vars.groupOutlineColor = "#E50E0E";
      } else {
          vars.groupOutlineColor = "#414141";
      }
  };

  thresholdPainter = function(opts, params, vars) {
      var threshold = params.group.threshold;
      var deficiency = params.group.deficiency_plots;
      if (threshold != "") {
        if (threshold == -1) {
          // Make neutral groups grey
          vars.groupColor.s = 0;
          vars.groupColor.l = 54;
          vars.groupColor.h = 11;
        } else if (threshold < 50) {
          vars.groupColor.s = 100;
          vars.groupColor.l = 50;
          vars.groupColor.h = 0 //threshold * .5;
        } else if (threshold >= 50 && threshold < 75) {
          vars.groupColor.s = 100;
          vars.groupColor.l = 50;
          vars.groupColor.h = 45 //threshold * .75;
        } else if (threshold >= 75) {
          vars.groupColor.s = 100;
          vars.groupColor.l = 35;
          vars.groupColor.h = 100;
        }
      }
      // vars.groupColor.model = "hsla";
  };

  var labelUpdate = function(group) {
    if (!group) {
      var selection = circles.get("selection").groups;
      if (selection.length > 0) {
        group = selection[0];
      }
    }

    if (group) {
      $("#titlebar").addClass("shown");
      $("#label").addClass("shown").text(group.label);
      $("#description").addClass("shown").text(group.description);
//            if (group.type == 'COI'){
//                $("#description").addClass("shown").text('');
//            }
      $("#result").addClass("shown").text('Result: ' + group.result);
      $("#test_notes").addClass("shown").text(group.test_notes);
      if (group.deficiency_text != null && group.deficiency_text != '') {
            $("#deficiency").addClass("shown")
            $("#deficiency_heading").addClass("shown").text("Deficiency");
            $("#deficiency_text").addClass("shown").text(group.deficiency_text);
      }
      else {
            $("#deficiency_text").removeClass("shown").text('');
            $("#deficiency_heading").removeClass("shown").text('');
      }
    } else {
      $("#titlebar").removeClass("shown");
    }
  };

  // this requests the file and executes a callback with the parsed result once
  //   it is available
  fetchJSONFile(demoData.a.data, function(data){
      var colorScheme = demoData.a.color;
      embedVisualization(data, colorScheme)
  });

  window.changeExample = function ( example ) {
    var colorScheme = demoData[example].color;
    var dataPath = demoData[example].data;
    fetchJSONFile(dataPath, function(data){
                embedVisualization(data, colorScheme)
            });
    $("#pageTitle").text(demoData[example].title);
  };

  window.reset = function() {
    window.currentId = null;
    var selectedGroups = circles.get("zoom")
    circles.set("zoom", {all: true, zoomed: false});
    circles.set('dataObject', window.dataObject);
  }

  window.upOneLevel = function() {
    function recursiveGroupSearch(group) {
      if (group.groups) {
        group.groups.forEach(function (innerGroup) {
          if (innerGroup.id === window.currentId) {
            window.currentId = group.id;
            circles.set('dataObject', group);
          }
          recursiveGroupSearch(innerGroup)
        })
      }
    }

    if (!window.currentId) return
    var match;
    window.dataObject.groups.forEach(function (group) {
      if (group.id === window.currentId) {
        window.reset()
      }
      recursiveGroupSearch(group)
    });
  }

  function updateData (e) {
    if (shiftKey) {
      if (e.group.groups) {
        window.currentId = e.group.id
        circles.set('dataObject', e.group);
      }
    }
  }

  function embedVisualization( model, colorScheme) {
    // Embed the visualization and make it a global object for simplicity.
    window.currentId = null;
    window.dataObject = model;
    window.circles = new CarrotSearchCircles({
      rainbowStartColor: "hsla(  0, 100%, 50%, 1)",
      rainbowEndColor:   "hsla(360, 100%, 50%, 1)",
      groupColorDecorator: thresholdPainter,
      dataObject: model,
      //groupOutlineColor: outlinePainter,
      id: "visualization",
      modelChangeAnimations: "parallel",
      diameter: function(box) {
        return Math.min(box.height - 2 * (5 + 20 + 20 + 5), box.width);
      },
      // Draw curved labels less often for mobile devices -- they're resource-intense.
      noTexturingCurvature: 0.2,
      deferLabelRedraws: 0,
      zoomedFraction: .5,
      labelRedrawFadeInTime: 0,

      ringScaling: 1.3,
      pixelRatio: window.devicePixelRatio || 1,

      // Pick the web font as the default.
      //backgroundColor: "rgba(0, 0, 0, 0)",
      groupFontFamily: "Helvetica",
      labelColorThreshold: 0,
      visibleGroupCount: 0,
      expanderAngle: 7,
      textureMappingMesh: false,
      attributionPositionX: "1%",
      attributionPositionY: "95%",
      // Decorate labels to uppercase (looks better with Open Sans Condensed).
      groupLabelDecorator: function(options, params, variables) {
        variables.labelText = variables.labelText //.toLocaleUpperCase();
      },
      groupHoverOutlineColor: "#00f",
      groupHoverOutlineWidth: 3,

      // Wire up events
      onGroupClick: function(e) {
        clickOn(e.group.id);
        labelUpdate(e.group);
        updateData(e)
      },
      onBeforeZoom: function(e) { return true; },
      onBeforeSelection: function(e) { return false; },
      onGroupSelectionChanged: function(e) {labelUpdate();},
      onGroupHover: function(evt) {labelUpdate(evt.group)},
      onLayout: function(e) {},
      onModelChanged: function(e) {
        $("#loading").hide();
      },
      onRolloutStart: function() {}
    });
  }
});
